% ============================================================
% Construction d'une base reduite par la methode POD 
% pour l'equation de Poisson 2D, avec conditions de
% Neumann et de Dirichlet sur le bord
%  
% avec 2 parametres:
%   * kappa1 (coeff diffusion dans ss-domaine \Omega1)
%   * kappa2 (coeff diffusion dans ss-domaine \Omega2)
%
% Note: le coeff diffusion dans le ss-domaine \Omega0 est fixe
% ============================================================

close all;
clear all;

% -------------------------
% definitions 
% -------------------------
% bornes du domaine parametrique
kappa1_min = 0.1;
kappa1_max = 1;
kappa2_min = 0.1;
kappa2_max = 1;
type_plan_experience = 'random'; % 'cartesien' ou 'random'
sauvegarde_base_reduite = true;    % true pour sauvegarder la base reduite
                                    % PP dans un fichier "PP.mat"
n_train = 35; % nombre de valeurs des paramètres mu à résoudre
sqrt_n_train = sqrt(n_train);
dkappa1 = (kappa1_max-kappa1_min)/(sqrt_n_train-1); % dkappa1 pour la grille carténsienne
dkappa2 = (kappa2_max-kappa2_min)/(sqrt_n_train-1); % dkappa2 pour la grille carténsienne

N = 20; % taille de la base réduite finale

% -------------------------
% construction du maillage
% -------------------------
nx = 200;
ny = 40;
mesh = MESH_build_cartesian(nx, ny);

% assemblages des matrices EF
% ---------------------------
[ DofNodes, AA_ref, LL_ref,...
      MM, DDX, DDY, BB, AA_decomp, LL_decomp] = FE_assemblages(mesh);

% nombre de degres de liberte
NbDof = size(DofNodes,1);

disp('--------------------');
disp(' Phase Exploratoire ');
disp('--------------------');
tic;

% definition du plan d'experience 
% --------------------------------
% on construit tous les couples (kappa1, kappa2)
% pour lesquels le probleme EF sera resolu (n_train couples en tout)
mu_list = []; % (taille n_train x 2)
if (strcmp(type_plan_experience, 'cartesien'))
    for i=0:sqrt_n_train-1
        for j=0:sqrt_n_train-1
            mu_list = [mu_list; [kappa1_min+i*dkappa1, kappa2_min+j*dkappa2]];
        end
    end
elseif (strcmp(type_plan_experience,'random'))
    for i=1:n_train
        mu_list = [mu_list; [kappa1_min+(kappa1_max-kappa1_min)*rand(), kappa2_min+(kappa2_max-kappa2_min)*rand()]];
    end
else
    error('type_plan_experience pas bien defini')
end

% resolution EF pour tout les couples (kappa1, kappa2) 
% -------------------------------------------------
n_train = size(mu_list,1);
disp(sprintf('Nbre de resolutions HF = %i', n_train));
AllUUs = zeros(NbDof,n_train);
for j=1:n_train
    % valeur du parametre
    mu_j = mu_list(j,:); 
    % calcul de la solution HF
    UU = PARAMETRIC_solve( mu_j, AA_decomp, LL_decomp );
    % on sauvegarde la solution dans la colonne j de AllUUs
    AllUUs(:,j) = UU;
end

elapsed = toc;
disp(sprintf('Phase Exploration elapsed time = %f s', elapsed));

disp('-------------------');
disp(' Phase Compression ');
disp('-------------------');
tic;

CC = 1/n_train * transpose(AllUUs)*BB*AllUUs;
[VV, D] = eig(CC);

elapsed = toc;
disp(sprintf('Phase Compression elapsed time = %f s', elapsed));

% Courbes de decroissance des valeurs propres
% ------------------------------------------------
figure;
semilogy(1:n_train, real(diag(D)), 'o');
xlabel('Rank of eigenvalues');
ylabel('Eigenvalues');
title('Distribution of eigenvalues');
grid on;

% Troncature et definition d'une base reduite
% -------------------------------------------
ZZ = AllUUs * VV(:,1:N);
PP = zeros(NbDof, N); % matrice (taille NbDof x N) representant une base reduite de taille N
% Processus de Gram-Schmidt pour orthonormaliser les vecteurs de ZZ
PP(:,1) = LINALG_orthonormalize(AllUUs(:,1), [], BB);
for i=2:N
    PP(:,i) = LINALG_orthonormalize(AllUUs(:,i), PP(:,1:i-1), BB);
end

% Visualisation des 5 premièrs vecteurs de la base reduite
UU_for_visu = zeros(mesh.NbNodes, 1);
UU_for_visu(DofNodes) = PP(:,1);
FE_visu(UU_for_visu, mesh, 'Solution PP1');
UU_for_visu(DofNodes) = PP(:,2);
FE_visu(UU_for_visu, mesh, 'Solution PP2');
UU_for_visu(DofNodes) = PP(:,3);
FE_visu(UU_for_visu, mesh, 'Solution PP3');
UU_for_visu(DofNodes) = PP(:,4);
FE_visu(UU_for_visu, mesh, 'Solution PP4');
UU_for_visu(DofNodes) = PP(:,5);
FE_visu(UU_for_visu, mesh, 'Solution PP5');

% Sauvergarde de la base reduite construite
% -----------------------------------------
if(sauvegarde_base_reduite)
    disp('Sauvegarde de la base reduite dans le fichier PP_pod.mat');
    save('PP_pod.mat', 'PP');
end