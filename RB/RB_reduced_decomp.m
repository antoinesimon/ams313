function [ Arb_decomp, Lrb_decomp ] = RB_reduced_decomp( AA_decomp, LL_decomp, PP )
% RB_reduced_decomp renvoie la décomposition de AA de la base réduite
% qui sont des matrices de taille N*N

Arb_decomp = cell(size(AA_decomp));
Lrb_decomp = cell(size(LL_decomp));

% On projette les matrices de la décomposition du problème complet sur la base réduite
for i=1:(size(AA_decomp))
    Arb_decomp{i} = PP' * AA_decomp{i} * PP;
end

for i=1:(size(LL_decomp))
    Lrb_decomp{i} = PP' * LL_decomp{i};
end

end