function [ Xrb ] = RB_solve( mu, Arb_decomp, Lrb_decomp )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

[ N_reduced, ~ ] = size(Arb_decomp{1});

AA_reduced = zeros(N_reduced, N_reduced);
thetaA = PARAMETRIC_thetaA(mu);
LL_reduced = zeros(N_reduced, 1);
thetaL = PARAMETRIC_thetaL(mu);

% Assemble the reduced matrices
for i=1:size(Arb_decomp)
    AA_reduced = AA_reduced + thetaA(i)*Arb_decomp{i};
    LL_reduced = LL_reduced + thetaL(i)*Lrb_decomp{i};
end

% Solve the reduced system
Xrb = AA_reduced \LL_reduced;

end