%================================================================
% Résolution du problème avec une base réduite contruite par POD dans la fichier MAIN_RBPOD.m
% On fait la résolution pout un nombre n_trial de valeurs du couple mu = (kappa1, kappa2) tirés aléatoirement
% et on calcule l'erreur entre la solution HF et la solution réduite ainsi que l'erreur maximale et moyenne
% sur l'échantillon de n_trial valeurs de mu.
%================================================================

% -------------------------
% construction du maillage
% -------------------------
nx = 200;
ny = 40;
mesh = MESH_build_cartesian(nx, ny);

% assemblages des matrices EF
% ---------------------------
[ DofNodes, AA_ref, LL_ref,...
      MM, DDX, DDY, BB, AA_decomp, LL_decomp] = FE_assemblages(mesh);

% nombre de degres de liberte
NbDof = size(DofNodes,1);

% -------------------------
% definitions 
% -------------------------
% bornes du domaine parametrique
kappa1_min = 0.1;
kappa1_max = 1;
kappa2_min = 0.1;
kappa2_max = 1;
% on charge la base réduite
load('PP_pod.mat', 'PP');
% nmbre de paramètres à tirer
n_trial = 100;
% on construit les matrices de décomposition de AA et de LL
[ Arb_decomp, Lrb_decomp ] = RB_reduced_decomp( AA_decomp, LL_decomp, PP );
% on initialise le vecteur de solutions réduites
[ ~, N] = size(PP);
Xrb = zeros(N, 1);
% on initialise les vecteurs d'erreurs
err_H1_list = zeros(n_trial, 1);

for i = 1:n_trial
    % on tire aléatoirement les paramètres
    kappa1 = kappa1_min + (kappa1_max - kappa1_min)*rand;
    kappa2 = kappa2_min + (kappa2_max - kappa2_min)*rand;
    mu = [kappa1, kappa2];

    % on résout le problème avec la base réduite
    Xrb = RB_solve( mu, Arb_decomp, Lrb_decomp );
    UU_reduced = PP * Xrb;
    % ajout des noeuds de dirichlet
    UU_reduced_full = FE_add_Dirichlet_DoFs( UU_reduced, mesh , DofNodes);

    %on résout le problème HF
    UU_ref = PARAMETRIC_solve(mu, AA_decomp, LL_decomp); 
    % ajout des noeuds de dirichlet
    UU_ref_full = FE_add_Dirichlet_DoFs( UU_ref, mesh , DofNodes);

    % approximation des derivees /x
    UUX = -DDX*UU_ref_full;
    UU_DX = MM \ UUX;
    UU_reduced_X = -DDX*UU_reduced_full;
    UU_reduced_DX = MM \ UU_reduced_X;
    % approximation des derivees /y
    UUY = -DDY*UU_ref_full;
    UU_DY = MM \ UUY;
    UU_reduced_Y = -DDY*UU_reduced_full;
    UU_reduced_DY = MM \ UU_reduced_Y;

    err_H1_list(i) = sqrt((UU_DX(DofNodes) - UU_reduced_DX(DofNodes))' * BB * (UU_DX(DofNodes) - UU_reduced_DX(DofNodes)) + (UU_DY(DofNodes) - UU_reduced_DY(DofNodes))' * BB * (UU_DY(DofNodes) - UU_reduced_DY(DofNodes)) + (UU_ref - UU_reduced)' * BB * (UU_ref - UU_reduced)) / sqrt(size(DofNodes, 1));
end

% on calcule l'erreur maximale et moyenne
err_max = max(err_H1_list);
disp(['erreur maximale = ', num2str(err_max)]);
err_moyenne = mean(err_H1_list);
disp(['erreur moyenne = ', num2str(err_moyenne)]);

