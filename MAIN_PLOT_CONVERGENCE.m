% on affiche les résultats des erreurs pour N = 4, 8, 12, 16, 20, 24, 28,
% 32 pour voir l'ordre de convergence de la méthode

NN = [4, 8, 12, 16, 20, 24, 28, 32];
NN_1 = [1/4, 1/8, 1/12, 1/16, 1/20, 1/24, 1/28, 1/32];
NN_6 = [1/4^6, 1/8^6, 1/12^6, 1/16^6, 1/20^6, 1/24^6, 1/28^6, 1/32^6];
NN_7 = [1/4^7, 1/8^7, 1/12^7, 1/16^7, 1/20^7, 1/24^7, 1/28^7, 1/32^7];
NN_8 = [1/4^8, 1/8^8, 1/12^8, 1/16^8, 1/20^8, 1/24^8, 1/28^8, 1/32^8];
NN_9 = [1/4^9, 1/8^9, 1/12^9, 1/16^9, 1/20^9, 1/24^9, 1/28^9, 1/32^9];

figure;
loglog(NN, [0.012, 0.011, 5.9e-5, 1.5e-5, 1.75e-6, 1e-7, 6.9e-8, 5.9e-9], 'o');
hold on
loglog(NN, NN_9*5e5);
hold off
xlabel('N');
ylabel('Erreur (norme H1)');
title("Convergence en fonction de l'erreur moyenne");
legend('Erreur mesurée','Référence de pente 9');
grid on;

figure;
loglog(NN, [0.06, 0.086, 0.0005, 0.00027, 3.6e-5, 2.2e-6, 1.5e-6, 8.8e-8], 'o');
hold on
loglog(NN, NN_9*1e7);
hold off
xlabel('N');
ylabel('Erreur (norme H1)');
title("Convergence en fonction de l'erreur maximale");
legend('Erreur mesurée','Référence de pente 9');
grid on;