close all;
clear all;

% bornes du domaine parametrique
kappa1_min = 0.1;
kappa1_max = 1;
kappa2_min = 0.1;
kappa2_max = 1;

% on tire au hasard un nombre n_kappa de paramètres
n_kappa = 3000;

% on charge la base réduite et les données nécessaires aux calcul efficace
% et stable du résidu
load('PP_greedy.mat', 'PP', 'Rmat', 'f_parallel', 'f_perp', 'mesh', ...
    'DofNodes', 'AA_ref', 'LL_ref', 'MM', 'DDX', 'DDY', 'BB', ...
    'AA_decomp', 'LL_decomp');
[ ~ , N ] = size(PP);

% nombre de degres de liberte
NbDof = size(DofNodes,1);

% on construit les matrices de décomposition de AA et de LL
[ Arb_decomp, Lrb_decomp ] = RB_reduced_decomp( AA_decomp, LL_decomp, PP );
Respart_ll = RB_compute_respart_ll( LL_decomp, BB );
[ Respart_la, Respart_aa ] = RB_compute_respart_la_aa( AA_decomp,LL_decomp, PP, BB );

% on calcul le vecteur JJ et JJ_rb et la norme de JJ
[ JJ ] = FE_quantity_of_interest( mesh, DofNodes );
JJ_intermediaire = BB \ JJ;
JJ_norm = sqrt(JJ_intermediaire'*BB*JJ_intermediaire);
JJ_rb = PP' * JJ;

% on calcule la constante de Poincaré discrète
AA_laplacien = sparse(NbDof, NbDof);
for i=1:length(AA_decomp)
    AA_laplacien = AA_laplacien + AA_decomp{i};
end
Cp = 1. / LINALG_invlanczos_min_eig(AA_laplacien, BB);
disp(sprintf('Constante de Poincaré = %i', Cp));

% on initialise le vecteur de solutions réduites
Xrb = zeros(N, 1);

% on initialise les vecteurs nécessaires
Qrb = zeros(n_kappa, 1);
delta_rb = zeros(n_kappa, 1);
kappa1_list = zeros(n_kappa, 1);
kappa2_list = zeros(n_kappa, 1);

%% calcul de Qrb
tic;

for i=1:n_kappa
    % on construit le paramètre courant
    kappa1_list(i) = kappa1_min+(kappa1_max-kappa1_min)*rand();
    kappa2_list(i) = kappa2_min+(kappa2_max-kappa2_min)*rand();
    mu = [ kappa1_list(i), kappa2_list(i) ];

    % on résout le problème avec la base réduite
    Xrb = RB_solve( mu, Arb_decomp, Lrb_decomp );

    % on calcule Q_rb
    Qrb(i) = JJ_rb' * Xrb;
end

elapsed_time = toc;
disp(['Elapsed time for Qrb: ', num2str(elapsed_time), ' seconds']);

%% calcul de delta_rb
tic;

for i=1:n_kappa
    % on récupère le paramètre courant
    mu = [ kappa1_list(i), kappa2_list(i) ];

    % on résout le problème avec la base réduite
    Xrb = RB_solve( mu, Arb_decomp, Lrb_decomp );

    % on calcule delta_rb
    alpha = min(mu) / (1+Cp*Cp);
    delta_rb(i) = JJ_norm*sqrt(RB_compute_resnorm2_stabilized(mu, Xrb,Rmat, f_parallel, f_perp))/alpha;
end

elapsed_time = toc;
disp(['Elapsed time for delta_rb: ', num2str(elapsed_time), ' seconds']);

%% figures
figure;
scatter(kappa1_list, kappa2_list, 10, Qrb, 'filled');
xlabel('\kappa_1');
ylabel('\kappa_2');
zlabel("Q^{rb}");
title("Q^{rb}");
colorbar;

figure;
scatter(kappa1_list, kappa2_list, 10, delta_rb, 'filled');
xlabel('\kappa_1');
ylabel('\kappa_2');
zlabel("\delta^Q_{rb}");
title("\delta^Q_{rb}");
colorbar;
