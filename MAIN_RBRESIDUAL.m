close all;
clear all;

% -------------------------
% construction du maillage
% -------------------------
nx = 200;
ny = 40;
mesh = MESH_build_cartesian(nx, ny);

% assemblages des matrices EF
% ---------------------------
[ DofNodes, AA_ref, LL_ref,...
      MM, DDX, DDY, BB, AA_decomp, LL_decomp] = FE_assemblages(mesh);

% nombre de degres de liberte
NbDof = size(DofNodes,1);

% bornes du domaine parametrique
kappa1_min = 0.1;
kappa1_max = 1;
kappa2_min = 0.1;
kappa2_max = 1;

% on charge la base réduite et sa dimension
load('PP_pod.mat', 'PP');
[ ~ , N ] = size(PP);

% nmbre de paramètres à tirer
n_trial = 100;

% on construit les matrices de décomposition de AA et de LL
[ Arb_decomp, Lrb_decomp ] = RB_reduced_decomp( AA_decomp, LL_decomp, PP );
Respart_ll = RB_compute_respart_ll( LL_decomp, BB );
[ Respart_la, Respart_aa ] = RB_compute_respart_la_aa( AA_decomp,LL_decomp, PP, BB );

% on initialise le vecteur de solutions réduites
Xrb = zeros(N, 1);

% on initialise les vecteurs des erreurs sur le calcul des résidus
residual_err_list = zeros(n_trial, 1);

for i = 1:n_trial
    % on tire aléatoirement les paramètres
    kappa1 = kappa1_min + (kappa1_max - kappa1_min)*rand;
    kappa2 = kappa2_min + (kappa2_max - kappa2_min)*rand;
    mu = [kappa1, kappa2];

    % on reconstruit AA et LL à partir des décompositions
    AA_recomp = sparse(NbDof, NbDof);
    thetaA = PARAMETRIC_thetaA(mu);
    for q=1:length(AA_decomp)
        AA_recomp = AA_recomp + thetaA(q)*AA_decomp{q};
    end
    LL_recomp = zeros(NbDof,1);
    thetaL = PARAMETRIC_thetaL(mu);
    for q=1:length(LL_decomp)
        LL_recomp = LL_recomp + thetaL(q)*LL_decomp{q};
    end

    % on résout le problème avec la base réduite
    Xrb = RB_solve( mu, Arb_decomp, Lrb_decomp );
    
    % on calcul le carré du résidu avec la méthode efficace
    res_eff = RB_compute_resnorm2(mu, Xrb, Respart_ll, Respart_la, Respart_aa);
    
    % on calcul le carré du résidu avec la méthode directe
    vec_res = AA_recomp*PP*Xrb-LL_recomp;
    res_dir = vec_res' * (BB \ vec_res);

    % on conserve l'erreur sur le calcul du carré du résidu entre la
    % méthode efficace et la méthode directe
    residual_err_list(i) = abs(res_dir - res_eff);
end

disp(mean(residual_err_list));
