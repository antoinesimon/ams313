%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% On affiche ici les erreurs d'approximation base réduite %
% sur l'espace des paramètres ainsi que les normes des    %
% résidus base réduite                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all;
clear all;

% -------------------------
% construction du maillage
% -------------------------
nx = 200;
ny = 40;
mesh = MESH_build_cartesian(nx, ny);

% assemblages des matrices EF
% ---------------------------
[ DofNodes, AA_ref, LL_ref,...
      MM, DDX, DDY, BB, AA_decomp, LL_decomp] = FE_assemblages(mesh);

% nombre de degres de liberte
NbDof = size(DofNodes,1);

% bornes du domaine parametrique
kappa1_min = 0.1;
kappa1_max = 1;
kappa2_min = 0.1;
kappa2_max = 1;

% maillage cartésien de l'espace des paramètres
n_kappa = 11;
d_kappa1 = (kappa1_max - kappa1_min)/(n_kappa-1);
d_kappa2 = (kappa2_max - kappa2_min)/(n_kappa-1);

% on charge la base réduite et sa dimension
load('PP_pod.mat', 'PP');
[ ~ , N ] = size(PP);

% on construit les matrices de décomposition de AA et de LL
[ Arb_decomp, Lrb_decomp ] = RB_reduced_decomp( AA_decomp, LL_decomp, PP );
Respart_ll = RB_compute_respart_ll( LL_decomp, BB );
[ Respart_la, Respart_aa ] = RB_compute_respart_la_aa( AA_decomp,LL_decomp, PP, BB );

% on initialise le vecteur de solutions réduites
Xrb = zeros(N, 1);

% on initialise les vecteurs des erreurs et des résidus
res_list = zeros(n_kappa, n_kappa);
err_list = zeros(n_kappa, n_kappa);

for i=0:n_kappa-1
    for j=0:n_kappa-1
        % on tire aléatoirement les paramètres
        kappa1 = kappa1_min + i*d_kappa1;
        kappa2 = kappa2_min + j*d_kappa2;
        mu = [kappa1, kappa2];
    
        % on reconstruit AA et LL à partir des décompositions
        AA_recomp = sparse(NbDof, NbDof);
        thetaA = PARAMETRIC_thetaA(mu);
        for q=1:3
            AA_recomp = AA_recomp + thetaA(q)*AA_decomp{q};
        end
        LL_recomp = zeros(NbDof,1);
        thetaL = PARAMETRIC_thetaL(mu);
        for q=1:3
            LL_recomp = LL_recomp + thetaL(q)*LL_decomp{q};
        end
    
        % on résout le problème avec la base réduite
        Xrb = RB_solve( mu, Arb_decomp, Lrb_decomp );
        
        % on calcul le carré du résidu avec la méthode efficace
        res_list(i+1,j+1) = sqrt(RB_compute_resnorm2(mu, Xrb, Respart_ll, Respart_la, Respart_aa));
        
        %% on calcul la solution HF et on reconstruit la solution RB, on
        % calcul la norme H1 de la différence
        UU_reduced = PP * Xrb;
        % ajout des noeuds de dirichlet
        UU_reduced_full = FE_add_Dirichlet_DoFs( UU_reduced, mesh , DofNodes);
    
        %on résout le problème HF
        UU_ref = PARAMETRIC_solve(mu, AA_decomp, LL_decomp); 
        % ajout des noeuds de dirichlet
        UU_ref_full = FE_add_Dirichlet_DoFs( UU_ref, mesh , DofNodes);
    
        % approximation des derivees /x
        UUX = -DDX*UU_ref_full;
        UU_DX = MM \ UUX;
        UU_reduced_X = -DDX*UU_reduced_full;
        UU_reduced_DX = MM \ UU_reduced_X;
        % approximation des derivees /y
        UUY = -DDY*UU_ref_full;
        UU_DY = MM \ UUY;
        UU_reduced_Y = -DDY*UU_reduced_full;
        UU_reduced_DY = MM \ UU_reduced_Y;
    
        err_list(i+1,j+1) = sqrt((UU_DX(DofNodes) - UU_reduced_DX(DofNodes))' * BB * (UU_DX(DofNodes) - UU_reduced_DX(DofNodes)) + (UU_DY(DofNodes) - UU_reduced_DY(DofNodes))' * BB * (UU_DY(DofNodes) - UU_reduced_DY(DofNodes)) + (UU_ref - UU_reduced)' * BB * (UU_ref - UU_reduced)) / sqrt(size(DofNodes, 1));
    end
end

[X, Y] = meshgrid(linspace(kappa1_min, kappa1_max, n_kappa), linspace(kappa2_min, kappa2_max, n_kappa));

figure;
surf(X, Y, err_list);
xlabel('\kappa_1');
ylabel('\kappa_2');
zlabel('erreur');
title("Erreur d'approximation base réduite");
view(2);
colorbar;

figure;
surf(X, Y, res_list);
xlabel('\kappa_1');
ylabel('\kappa_2');
zlabel('résidu');
title("Résidu base réduite");
view(2);
colorbar;
