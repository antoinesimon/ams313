close all;
clear all;

% bornes du domaine parametrique
kappa1_min = 0.1;
kappa1_max = 1;
kappa2_min = 0.1;
kappa2_max = 1;

% on tire les paramètres sur une grille cartésienne de taille
% n_kappa*n_kappa pour évaluer l'estimateur d'erreur et l'effectivité
n_kappa = 30;
d_kappa1 = (kappa1_max - kappa1_min)/(n_kappa-1);
d_kappa2 = (kappa2_max - kappa2_min)/(n_kappa-1);

% on charge la base réduite et les données nécessaires aux calcul efficace
% et stable du résidu
load('PP_greedy.mat', 'PP', 'Rmat', 'f_parallel', 'f_perp', 'mesh', ...
    'DofNodes', 'AA_ref', 'LL_ref', 'MM', 'DDX', 'DDY', 'BB', ...
    'AA_decomp', 'LL_decomp');
[ ~ , N ] = size(PP);

% nombre de degres de liberte
NbDof = size(DofNodes,1);

% on construit les matrices de décomposition de AA et de LL
[ Arb_decomp, Lrb_decomp ] = RB_reduced_decomp( AA_decomp, LL_decomp, PP );
Respart_ll = RB_compute_respart_ll( LL_decomp, BB );
[ Respart_la, Respart_aa ] = RB_compute_respart_la_aa( AA_decomp,LL_decomp, PP, BB );

% D'abord on calcule la constante de Poincaré discrète
AA_laplacien = sparse(NbDof, NbDof);
for i=1:length(AA_decomp)
    AA_laplacien = AA_laplacien + AA_decomp{i};
end
Cp = 1. / LINALG_invlanczos_min_eig(AA_laplacien, BB);
disp(sprintf('Constante de Poincaré = %i', Cp));

% on initialise le vecteur de solutions réduites
Xrb = zeros(N, 1);

% on initialise les vecteurs des estimateurs d'erreurs,des erreurs H1 et
% des effectivités
err_estim_list = zeros(n_kappa, n_kappa);
err_H1_list = zeros(n_kappa, n_kappa);
majo_effectivity_list = zeros(n_kappa, n_kappa);
effectivity_list = zeros(n_kappa, n_kappa);

for i=0:n_kappa-1
    for j=0:n_kappa-1
        % on construit le paramètre courant
        kappa1 = kappa1_min + i*d_kappa1;
        kappa2 = kappa2_min + j*d_kappa2;
        mu = [kappa1, kappa2];
    
        % on résout le problème avec la base réduite
        Xrb = RB_solve( mu, Arb_decomp, Lrb_decomp );
        
        %% d'abord on calcule l'estimateur d'erreur et l'effectivité
        % on calcule le carré du résidu avec la méthode efficace stable
        res_stab = sqrt(RB_compute_resnorm2_stabilized(mu, Xrb, Rmat, f_parallel, f_perp));
        
        % on calcule le coefficient alpha^{\mathcal(N)}(mu)
        alpha = min(kappa1, kappa2) / (1+Cp*Cp);

        % on en déduit l'estimateur d'erreur
        err_estim_list(i+1, j+1) = res_stab / alpha;

        % on a aussi l'effectivité
        majo_effectivity_list(i+1, j+1) = 1/alpha;

        %% on calcul la solution HF et on reconstruit la solution RB, on
        % calcul la norme H1 de la différence
        UU_reduced = PP * Xrb;
        % ajout des noeuds de dirichlet
        UU_reduced_full = FE_add_Dirichlet_DoFs( UU_reduced, mesh , DofNodes);
    
        %on résout le problème HF
        UU_ref = PARAMETRIC_solve(mu, AA_decomp, LL_decomp); 
        % ajout des noeuds de dirichlet
        UU_ref_full = FE_add_Dirichlet_DoFs( UU_ref, mesh , DofNodes);

        % approximation des derivees /x
        UUX = -DDX*UU_ref_full;
        UU_DX = MM \ UUX;
        UU_reduced_X = -DDX*UU_reduced_full;
        UU_reduced_DX = MM \ UU_reduced_X;
        % approximation des derivees /y
        UUY = -DDY*UU_ref_full;
        UU_DY = MM \ UUY;
        UU_reduced_Y = -DDY*UU_reduced_full;
        UU_reduced_DY = MM \ UU_reduced_Y;
    
        err_H1_list(i+1,j+1) = sqrt((UU_DX(DofNodes) - UU_reduced_DX(DofNodes))' * BB * (UU_DX(DofNodes) - UU_reduced_DX(DofNodes)) + (UU_DY(DofNodes) - UU_reduced_DY(DofNodes))' * BB * (UU_DY(DofNodes) - UU_reduced_DY(DofNodes)) + (UU_ref - UU_reduced)' * BB * (UU_ref - UU_reduced)) / sqrt(NbDof);
    
        %% on peut désormais calculer le véritable rapport entre l'estimateur et l'erreur réelle
        effectivity_list(i+1, j+1) = err_estim_list(i+1, j+1) / err_H1_list(i+1,j+1);
    end
end

min_eff = min(min(effectivity_list));
max_eff = max(max(effectivity_list));

disp(sprintf('effectivité minimale= %i', num2str(min_eff)));
disp(sprintf('effectivité maximale= %i', num2str(max_eff)));

[X, Y] = meshgrid(linspace(kappa1_min, kappa1_max, n_kappa), linspace(kappa2_min, kappa2_max, n_kappa));

figure;
surf(X, Y, err_estim_list);
xlabel('\kappa_1');
ylabel('\kappa_2');
zlabel("estimation de l'erreur");
title("Estimateur d'erreur en fonction de kappa");
view(2);
colorbar;

figure;
surf(X, Y, err_H1_list);
xlabel('\kappa_1');
ylabel('\kappa_2');
zlabel('erreur en norme H1');
title("Erreur réelle entre HF et RB");
view(2);
colorbar;

figure;
surf(X, Y, effectivity_list);
xlabel('\kappa_1');
ylabel('\kappa_2');
zlabel('effectivité mesurée');
title("Effectivité réelle");
clim([0, 15]);
view(2);
colorbar;

figure;
surf(X, Y, majo_effectivity_list);
xlabel('\kappa_1');
ylabel('\kappa_2');
zlabel('effectivité théorique');
title("Majoration théorique de l'effectivité");
view(2);
colorbar;
